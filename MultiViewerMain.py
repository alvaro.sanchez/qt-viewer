#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide.QtGui import *
from PySide import QtCore

from widgets.uiFiles import ui_MultiViewerMain as ui
from widgets import CameraWidget
from widgets import ControllersWidget
from common.CinemaSpec import *
from common import DatabaseLoader


class ViewerMain(QMainWindow):
    ''' Uses ControllerWidget and CameraWidget abstractions to support multiple
    side-by-side views.

    Receives a list of Cinema stores and creates a ControllerWidget-CameraWidget pair
    per store instance.  Parameters and color controls are fully independent per
    store (except background color which is controlled globally. '''
    def __init__(self, parent = None):
        super(ViewerMain, self).__init__(parent)
        self.__initializeViewer()

    def __initializeViewer(self):
        self.__ui = ui.Ui_ViewerMain()
        self.__cameraWidgets = []
        self.__currentController = None
        self.__currentCamera = None
        self.__colorPicker = QColorDialog(self)
        self.__fileDialog = QFileDialog(self, "Open a Cinema Database", "~",\
          "JSON Input Files (*.json)")

        # Setup ui
        self.__ui.setupUi(self)
        self.__ui.actionExit.triggered.connect(self.close)
        self.__ui.actionOpen.triggered.connect(self.__onOpenFileTriggered)
        self.__ui.pbBgColor.clicked.connect(self.__onBgColorClicked)

    @QtCore.Slot()
    def __onOpenFileTriggered(self):
        ''' Re-initializes the viewer. '''
        if self.__fileDialog.exec_() == self.__fileDialog.Accepted:
            filePath = self.__fileDialog.selectedFiles()[0]
            self.__initializeViewer()
            databases = DatabaseLoader.loadAll(filePath)
            self.setStores(databases)

    @QtCore.Slot()
    def __onBgColorClicked(self):
        if self.__colorPicker.exec_() == self.__colorPicker.Accepted:
            color = self.__colorPicker.selectedColor()

            for cw in self.__cameraWidgets:
                cw.setBackgroundColor(color)
                index = cw.getId()
                controller = self.__ui.stackedControllers.widget(index)
                controller.triggerDisplayUpdate()

    @QtCore.Slot(int)
    def __onCameraSelected(self, cameraId):
        ''' Handles selection (mouse press) of different CameraWidget. Switches
        between ControllerWidgets, etc. '''
        # self.sender() does not work properly so the previously set Id is used
        # to identify the calling camera view.
        if self.__ui.stackedControllers.currentIndex() is not cameraId:

            camera = self.__cameraWidgets[cameraId]
            if (self.__currentCamera):
                self.__currentCamera.setFrameShape(QFrame.NoFrame)
            camera.setFrameShape(QFrame.Panel)
            self.__currentCamera = camera

            self.__ui.stackedControllers.setCurrentIndex(cameraId)
            controller = self.__ui.stackedControllers.currentWidget()
            self.__currentController = controller

    def __createCameraAndControllers(self, store):
        spec = CinemaSpec().resolveStoreVersion(store)

        # Create camera widget
        cw = CameraWidget.CameraWidget(self)
        cw.setSpecification(spec)
        self.__ui.layoutCameras.addWidget(cw)
        self.__cameraWidgets.append(cw)

        # Create controller widget. Use the ControllersWidget index in the
        # QStackedWidget as a CameraWidgetId.
        control = ControllersWidget.ControllersWidget(self)
        cameraId = self.__ui.stackedControllers.addWidget(control)
        control.setStore(store, spec, cw, cameraId)

        # Connect camera widget to the global switch
        cw.cameraSelected.connect(self.__onCameraSelected)

    def setStores(self, databases):
        for cs in databases:
            self.__createCameraAndControllers(cs)

        # Force selection update to render the frame
        if len(databases) > 1:
            self.__ui.stackedControllers.setCurrentIndex(1)
            self.__onCameraSelected(0)
