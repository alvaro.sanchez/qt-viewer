#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide.QtGui import *
from PySide.QtCore import *

class QColorBar(QWidget):
    def __init__(self, parent=None):
        super(QColorBar, self).__init__(parent)
        self.__lut = None
        self.__x = None
        self._minMax = tuple([None, None])

    def resizeEvent(self, e):
        self.repaint()

    def setMinMax(self, minMax):
        self._minMax = minMax

    def setLookupTable(self, lookup_table):
        self.__lut = lookup_table.lut
        self.__x = lookup_table.x
        self.update()

    def paintEvent(self, e):
        painter = QPainter(self)

        if not self.__lut is None and \
           not self.__x is None:

            colorCount = len(self.__lut)
            fullWidth = self.rect().width()
            height = self.rect().height() / 1

            xs = self.__x
            if xs[-1] == 1.0:
                # Need to make some space for the 1.0 color
                P = 0.1
                for i in range(0, len(xs)):
                    xs[i] = xs[i] - i * P / colorCount

            # Render the color bar
            for i in range(0, colorCount):
                # Render the color block
                x = xs[i]
                if i == colorCount - 1:
                    nextX = 1.0
                else:
                    nextX = xs[i + 1]
                blockWidth = fullWidth * (nextX - x)
                startX = fullWidth * x
                blockRect = QRect(startX, 0, startX + blockWidth, height)
                color = QColor(self.__lut[i][0], \
                    self.__lut[i][1], \
                    self.__lut[i][2])
                painter.fillRect(blockRect, color)
        else:
            painter.fillRect(self.rect(), QColor(236, 236, 236))
