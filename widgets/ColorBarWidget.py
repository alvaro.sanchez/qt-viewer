import sys
import os

from rendering.LookupTableManager import *

from PySide import QtCore
from PySide.QtCore import *
from PySide.QtGui import *
from uiFiles import ui_ColorBarWidget as ui


class ColorBarWidget(QWidget):
    colorLutChanged = Signal(tuple)

    def __init__(self, parent):
        super(ColorBarWidget, self).__init__(parent)
        self._minStr = ""
        self._maxStr = ""

        self._ui = ui.Ui_ColorBarWidget()
        self._ui.setupUi(self)

        # load luts, populate combo box and setup qcolorbar
        self._cLutManager = LookupTableManager()
        self._cLutManager.read(os.path.dirname(sys.argv[0]) + '/builtin_tables.json')
        menu = self._ui._cbLutMenu
        menu.addItems(self._cLutManager.names())
        self._ui._colorBar.setLookupTable(self._cLutManager.getColorLutStructByName("None"))

        # connect ui signals
        menu.currentIndexChanged.connect(self._onLutChanged)
        self._ui._pbImport.clicked.connect(self._onImportTable)

    def _updateRangeLabels(self, currentLut):
        if (currentLut != "None" and currentLut != ""):
            self._ui._labelMin.setText(self._minStr)
            self._ui._labelMax.setText(self._maxStr)
        else:
            self._ui._labelMin.setText("")
            self._ui._labelMax.setText("")

    def _onLutChanged(self):
        currentLut = self._ui._cbLutMenu.currentText()
        clut = self._cLutManager.getColorLutStructByName(currentLut)
        self._ui._colorBar.setLookupTable(clut)
        self.colorLutChanged.emit(tuple([clut]))

        self._updateRangeLabels(currentLut)

    def _onImportTable(self):
        dialog = QFileDialog(self,
            "Import Lookup Table", "", 'JSON Files (*.json)')

        if  dialog.exec_() == dialog.Accepted:
            files = dialog.selectedFiles()
            if files and len(files) > 0:
                self._cLutManager.read(files[0])
                self._ui._cbLutMenu.clear()
                self._ui._cbLutMenu.addItems(self._cLutManager.names())

    def getCurrentColorLut(self):
        currentName = self._ui._cbLutMenu.currentText()
        return self._cLutManager.getColorLutStructByName(currentName)

    @QtCore.Slot(tuple)
    def setMinMaxValues(self, minMax):
        self._minStr = ('%0.1f' % minMax[0]) if minMax[0] != None else ""
        self._maxStr = ('%0.1f' % minMax[1]) if minMax[1] != None else ""

        self._updateRangeLabels(self._ui._cbLutMenu.currentText())

    @QtCore.Slot(str)
    def setCurrentLut(self, name):
        index = self._ui._cbLutMenu.findText(name)
        if index != -1:
            self._ui._cbLutMenu.setCurrentIndex(index)
