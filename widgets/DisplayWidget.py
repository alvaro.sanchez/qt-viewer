from PySide.QtCore import *
from PySide.QtGui import *
from uiFiles import ui_DisplayWidget as ui
from rendering.RenderViewMouseInteractor import *
from rendering.compositor import *
from common.CinemaSpec import *


class DisplayWidget(QFrame):
    phiChanged = Signal(int)
    thetaChanged = Signal(int)

    def __init__(self, parent):
        super(DisplayWidget, self).__init__(parent)
        self._ui = ui.Ui_DisplayWidget()
        self._mouseInteractor = RenderViewMouseInteractor()
        self._ui.setupUi(self)

    def setSpecification(self, spec):
        if (spec is CinemaSpec.A):
            self.__compositor = Compositor_SpecA()
            bgcolor = QColor(0, 0, 0, 255)
        elif (spec is CinemaSpec.B):
            self.__compositor = Compositor_SpecB()
            bgcolor = QColor(80, 81, 109, 255)

        self.setBackgroundColor(bgcolor)

    def _updateCamera(self):
        scale = self._mouseInteractor.getScale()
        self._ui._renderView.resetTransform()
        self._ui._renderView.scale(scale, scale)

        # emit signals to sync with controller (e.g. slider)
        phi   = self._mouseInteractor.getPhi()
        theta = self._mouseInteractor.getTheta()

        self.phiChanged.emit(phi)
        self.thetaChanged.emit(theta)

    def connectInteractorSignals(self):
        rv = self._ui._renderView
        mi = self._mouseInteractor

        rv.mousePressSignal.connect(mi.onMousePress)
        rv.mouseMoveSignal.connect(mi.onMouseMove)
        rv.mouseReleaseSignal.connect(mi.onMouseRelease)
        rv.mouseWheelSignal.connect(mi.onMouseWheel)
        rv.mouseMoveSignal.connect(self._updateCamera)
        rv.mouseWheelSignal.connect(self._updateCamera)

    def disconnectInteractorSignals(self):
        try:
            rv = self._ui._renderView
            mi = self._mouseInteractor

            rv.mousePressSignal.disconnect(mi.onMousePress)
            rv.mouseMoveSignal.disconnect(mi.onMouseMove)
            rv.mouseReleaseSignal.disconnect(mi.onMouseRelease)
            rv.mouseWheelSignal.disconnect(mi.onMouseWheel)
            rv.mouseMoveSignal.disconnect(self._updateCamera)
            rv.mouseWheelSignal.disconnect(self._updateCamera)
        except:
            # No big deal if we can't disconnect
            print "Failed to disconnect interactor signals"
            pass

    def initializePhiValues(self, param):
        self._mouseInteractor.setPhiValues(param["values"])
        self._mouseInteractor.setPhi(param["default"])

    def initializeThetaValues(self, param):
        self._mouseInteractor.setThetaValues(param["values"])
        self._mouseInteractor.setTheta(param["default"])

    def setPhi(self, value):
        self._mouseInteractor.setPhi(value)

    def setTheta(self, value):
        self._mouseInteractor.setTheta(value)

    def renderView(self):
        return self._ui._renderView

    @property
    def compositor(self):
        return self.__compositor

    @QtCore.Slot('QColor')
    def setBackgroundColor(self, color):
        self.__compositor.set_background_color(color.getRgb())
        self._ui._renderView.setBackgroundBrush(QBrush(color))

    @QtCore.Slot(bool)
    def enableGeometryColor(self, enable):
        self.__compositor.enableGeometryColor(enable)

    @QtCore.Slot(bool)
    def enableLighting(self, enable):
        self.__compositor.enableLighting(enable)
