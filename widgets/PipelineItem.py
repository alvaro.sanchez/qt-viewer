from PropertyModel import *

# TODO improve the visibility display role (pixmap?)
class PipelineItem:
    ''' Item used by the PipelineModel.  Serves as container for different specific
    properties of an element in the Pipeline tree (visibility (and whether it is
    enabled for an item or not), child items, parent, color luts (through propertyItem)).

    Convention: _displayData[0] holds the displayed parameter name, _displayData[1] holds
    the visibility value displayed'''
    def __init__(self, displayData, parent = None):
        self._childItems = []
        self._displayData = displayData
        self._parentItem = parent
        self._visibility = False
        self._visibilitySelection = False if displayData[1] == "" else True
        self._radioButtonBehavior = False
        self._dbParameterName = displayData[0]
        # temporary container for luts and colors; will become an item in a Property Model
        self.__propertyItem = None if displayData[1] == "" else PropertyItem()

    def appendChild(self, child):
        self._childItems.append(child)
        child._parentItem = self

    def child(self, row):
        return self._childItems[row]

    def childCount(self):
        return len(self._childItems)

    def columnCount(self):
        return len(self._displayData)

    def data(self, column):
        return self._displayData[column]

    def setData(self, column, value):
        self._displayData[column] = value

    def row(self):
        if self._parentItem:
            return self._parentItem._childItems.index(self)

        return 0

    def parent(self):
        return self._parentItem

    # TODO Would be nice to also change the display role directly. Would this be possible
    # without doing it through Model::setData? (probably not since dataChanged needs to be emitted
    # to update the view). What would be the best way to do this while keeping it easy to
    # exchange the display role for visibility (e.g. instead of x/yes, a pixmap)
    def toggleVisibility(self):
        self._visibility = not self._visibility
        return self._visibility

    def isVisible(self):
        return self._visibility

    def isVisibilitySelectionEnabled(self):
        return self._visibilitySelection

    def setParameterName(self, name):
        self._dbParameterName = name

    def propertyItem(self):
        return self.__propertyItem
