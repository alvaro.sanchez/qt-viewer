#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide.QtCore import *
from rendering.LookupTableManager import LookupTable


class PropertyItem:
    ''' Container for specific rendering options (geometry color, color lut, etc.)
    of a parameter's property (e.g. [Parameter]Calculator1 -> [Property]coord_x_2).
    This is intended to become the PropertyModel table item. '''
    def __init__(self, colorLut = LookupTable(), geometryColor = [255, 255, 255]):
        self.__colorLut = colorLut
        self.__geometryColor = geometryColor
        self.__valueRange = tuple([None, None])
        # Convention: [0]-> displayed param name; [1]-> displayed vis
        #self.__displayData = displayData

    def colorLut(self):
        return self.__colorLut

    def geometryColor(self):
        return self.__geometryColor

    def valueRange(self):
        return self.__valueRange

    def setColorLut(self, lut):
        self.__colorLut = lut

    def setGeometryColor(self, color):
        self.__geometryColor = color

    def setValueRange(self, valRange):
        self.__valueRange = valRange

#    def data(self, column):
#    def setData(self, column, value):
#    def columnCount(self):
#    def row(self):
