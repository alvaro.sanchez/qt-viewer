#!/usr/bin/python
import sys
import json
import os

from PySide.QtCore import *
from PySide.QtGui import *

if os.path.exists("./widgets/uiFiles"):
  # compile ui files as long as the dir exists
  import pysideuic as uic
  uic.compileUiDir("./widgets/uiFiles")

from ViewerMain import *
from cinema_python import cinema_store


# create the qt app
a = QApplication(sys.argv)
v = ViewerMain()

# parse arguments if any
argv = sys.argv
if len(argv) > 1:
    # try to open up a store
    with open(sys.argv[1], mode="rb") as file:
        info_json = json.load(file)

    storeType = "MFS"
    try:
        if info_json["metadata"]["store_type"] == "SFS":
            cs = cinema_store.SingleFileStore(sys.argv[1])
        else:
            raise TypeError

    except(TypeError,KeyError):
        cs = cinema_store.FileStore(sys.argv[1])

    cs.load()
    v.setStore(cs)

## ------------------- start profiling -------------------------------
#import cProfile, pstats, StringIO
#pr = cProfile.Profile()
#pr.enable()
## -------------------------------------------------------------------

v.show()
qAppRet = a.exec_()

## --------------------- finish profiling ----------------------------
#pr.disable()
#s = StringIO.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats(0.1) # print 10% of calls
#print s.getvalue()
## -------------------------------------------------------------------

sys.exit(qAppRet)
