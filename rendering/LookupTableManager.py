#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import json
import numpy as np
import math


class LookupTable:
    ''' Color table container.
      - self.lut          : Actual color LUT.
      - self.x            : Vector containing the value bins.
      - self.adjustedBins : Vector containing the value bins (adjusted to hold a
        value > 1.0 in the end).
    '''
    def __init__(self):
        self.name = 'None'
        self.colorSpace = 'None'
        self.lut = None
        self.x = None
        self.adjustedBins = None

class LookupTableManager:
    '''Loads a set of color lookup tables from a *.json file. '''
    def __init__(self):
        self.luts = []
        self.lut = None
        self.x = None

        # Add a default color map ('None')
        self.luts.append(LookupTable())

    def read(self, file_path):
        with open(file_path) as json_file:
            data = json.load(json_file)
            for i in range(0, len(data)):
                lutentry = LookupTable()
                lutentry.name = data[i]['Name']
                lutentry.colorSpace = data[i]['ColorSpace']

                rgbPoints = data[i]['RGBPoints']
                xs = []
                tlut = []
                minx = float("inf")
                maxx = float("-inf")
                for i in xrange(0, len(rgbPoints),4):
                    x = rgbPoints[i]
                    maxx = x if x > maxx else maxx
                    minx = x if x < minx else minx
                    xs.append(rgbPoints[i])
                    rgb = (rgbPoints[i + 1], rgbPoints[i + 2], rgbPoints[i + 3])
                    tlut.append(rgb)

                # Remap xs from [minx, max] -> [0, 1]
                scale = maxx - minx
                xs = [(x - minx) / scale for x in xs]

                for i in range(0, len(tlut)):
                    tlut[i] = (tlut[i][0]*255, tlut[i][1]*255, tlut[i][2]*255)
                lut = np.array(tlut, dtype=np.uint8)
                lutentry.lut = lut
                lutentry.x = xs
                self.__addAdjustedBins(lutentry)
                self.luts.append(lutentry)

    def __addAdjustedBins(self, lutEntry):
        ''' Moved from compositor.__applyColorLut. '''
        # use a histogram to support non-uniform colormaps (fetch bin indices)
        colorLut = lutEntry.lut
        colorLutXValue = lutEntry.x
        bins = list(colorLutXValue)
        if colorLutXValue[-1] < 1.0:
            bins.append(1.0)
        else:
            bins.append(1.01) # 1.0 gets its own color so need an extra bin

        lutEntry.adjustedBins = bins

    def getColorLutStructByName(self, name):
        for lut in self.luts:
            if lut.name == name:
                return lut
        return None

    def names(self):
        """
        Return an array of the names of all available lookup tables.
        """
        names = []
        for lut in self.luts:
            names.append(lut.name)
        return names
