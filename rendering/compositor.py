#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import abc
import numpy as np


class Compositor(object):
    __metaclass__ = abc.ABCMeta
    ''' Base class for different compositor specifications.'''
    def __init__(self):
        self.__bgColor =  tuple([0, 0, 0, 0])

    @abc.abstractmethod
    def __renderImpl(self, layers):
        return

    def enableGeometryColor(self, enable):
        #print "Warning: geometry color not supported in this specification."
        return

    def enableLighting(self, enable):
        #print "Warning: lighting not supported in this specification."
        return

    def setColorDefinitions(self, colorDefs):
        #print "Warning: color definitions not supported in this specification."
        return

    def set_background_color(self, rgb):
        self.__bgColor = rgb

    def render(self, layers):
        return self.__renderImpl(layers)


class Compositor_SpecA(Compositor):
    ''' Compositor SpecA. Loads LayerSpecs consisting of a
    single image. No compositing supported.'''
    def __init__(self, parent = None):
        super(Compositor_SpecA, self).__init__()

    def _Compositor__renderImpl(self, layers):
        layer = layers[0] if (len(layers) > 0) else None

        if layer is None:
            raise IndexError ("There are no valid layers to render!")

        return layer.getColorArray()


class Compositor_SpecB(Compositor):
    ''' Compositor SpecB. Composites several LayerSpec instances together.
    Each LayerSpec holds all of its buffer components (depth, color, luminance,
    etc.).'''
    def __init__(self, parent = None):
        super(Compositor_SpecB, self).__init__()
        self.__geometryColorEnabled = False
        self.__lightingEnabled = True
        self.__colorDefinitions = {}

    def ambient(self, rgb):
        """ Returns the ambient contribution in an RGB luminance image. """
        return np.dstack((rgb[:,:,0], rgb[:,:,0], rgb[:,:,0]))
    def diffuse(self, rgb):
        """ Returns the diffuse contribution in an RGB luminance image. """
        return np.dstack((rgb[:,:,1], rgb[:,:,1], rgb[:,:,1]))
    def specular(self, rgb):
        """ Returns the specular contribution in an RGB luminance image. """
        return np.dstack((rgb[:,:,2], rgb[:,:,2], rgb[:,:,2]))

    def enableGeometryColor(self, enable):
        self.__geometryColorEnabled = enable

    def enableLighting(self, enable):
        self.__lightingEnabled = enable

    def setColorDefinitions(self, colorDefs):
        self.__colorDefinitions = colorDefs

    def __getCustomizedColorBuffer(self, layer):
        ''' Queries the user-defined color customizations from a dictionary and applies them to
        a specific LayerSpec instance. The color customizations form the ui are matched to its
        referred LayerSpec using the layer name (a 'parameter/value' tag), which is set in the
        QueryTranslator. See the PipelineModel for more information on the colorDefinitions struct.
        '''
        array = None
        customizationName = layer.customizationName
        if layer.hasValueArray():
            array = layer.getValueArray()

            if customizationName in self.__colorDefinitions:
                array = self.__applyColorLut(array, layer.getDepth(), self.__colorDefinitions[customizationName]["colorLut"])

        elif layer.hasColorArray():
            array = np.copy(layer.getColorArray())

            if customizationName in self.__colorDefinitions:
                self.__applyFillColor(array, layer.getDepth(), self.__colorDefinitions[customizationName])

        return array

    def __applyFillColor(self, array, depth, colorDef):
        ''' Applies the user defined geometry color to an array.
        TODO Performance is notoriously affected when calling this function.
        Needs optimization. '''
        if self.__geometryColorEnabled:
            # Process only foreground (object) values
            arrIdx = self.__getForegroundPixels(depth)
            if not arrIdx:
                return
            fill_color = colorDef["geometryColor"]
            array[arrIdx[0], arrIdx[1], :] = fill_color[0:3]

    def __applyColorLut(self, rgbVarr, depth, colorLutStruct):
        if colorLutStruct.name == "None":
            return rgbVarr
        # Process only foreground (object) values
        varrIdx = self.__getForegroundPixels(depth)
        if not varrIdx:
            return rgbVarr

        # Decode RGB to float
        w0 = np.left_shift(rgbVarr[varrIdx[0], varrIdx[1], 0].astype(np.uint32), 16)
        w1 = np.left_shift(rgbVarr[varrIdx[0], varrIdx[1], 1].astype(np.uint32), 8)
        w2 = rgbVarr[varrIdx[0], varrIdx[1], 2]

        value = np.bitwise_or(w0,w1)
        value = np.bitwise_or(value,w2)
        value = np.subtract(value.astype(np.int32),1) #0 is reserved as "nothing"
        normalized_val = np.divide(value.astype(float),0xFFFFFE)

        # Map float value to color lut (use a histogram to support non-uniform
        # colormaps (fetch bin indices))
        colorLut = colorLutStruct.lut
        colorLutXValue = colorLutStruct.x
        bins = colorLutStruct.adjustedBins

        idx = np.digitize(normalized_val, bins)
        idx = np.subtract(idx, 1)

        valueImage = np.zeros([rgbVarr.shape[0], rgbVarr.shape[1]], dtype = np.uint32)
        valueImage[varrIdx[0], varrIdx[1]] = idx

        return colorLut[valueImage]

    def __getForegroundPixels(self, depth):
        '''Computes the indices of the foreground object using the depth buffer. '''
        bgDepth = np.max(depth)
        indices = np.where(depth < bgDepth)
        if len(indices[0]) == 0:
            # Only background
            return None
        return indices

    def _Compositor__renderImpl(self, layers):
        """
        Takes an array of layers (LayerSpec) and composites them into an RGB image.
        """
        # find a valid LayerSpec (valid = at least one color loaded)
        # necessary for compatibility SpecB-testcase1
        l0 = None
        for index, layer in enumerate(layers):
            if layer.hasColorArray() or layer.hasValueArray():  # TODO might be necessary to check if the actual array is not NONE
                l0 = layer
                break

        if l0 is None:
            raise IndexError ("There are no valid layers to render!")

        c0 = self.__getCustomizedColorBuffer(l0)


        if self.__lightingEnabled:
            lum0 = l0.getLuminance()
            if not lum0 is None:
                # modulate color of first layer by the luminance
                lum0 = np.copy(lum0)
                lum0 = self.diffuse(lum0)
                c0[:,:,:] = c0[:,:,:] * (lum0[:,:,:]/255.0)

        # composite the rest of the layers
        d0 = np.copy(l0.getDepth())
        for idx in range(1, len(layers)):

            cnext = self.__getCustomizedColorBuffer(layers[idx])
            # necessary for compatibility Spect-testcase1
            if cnext is None:
                continue

            dnext = layers[idx].getDepth()
            lnext = layers[idx].getLuminance()

            # put the top pixels into place
            indices = np.where(dnext < d0)
            if (self.__lightingEnabled and lnext is not None):
                # modulate color by luminance then insert
                lnext = self.diffuse(lnext)
                c0[indices[0], indices[1], :] = \
                    cnext[indices[0], indices[1], :] * \
                    (lnext[indices[0], indices[1], :] / 255.0)
            else:
                # no luminance, direct insert
                c0[indices[0],indices[1],:] = cnext[indices[0],indices[1],:]

            d0[indices[0], indices[1]] = dnext[indices[0], indices[1]]


        if not (d0 is None) and not (d0.ndim == 0):
            #set background pixels to gray to avoid colormap
            #TODO: curious why necessary, we encode a NaN value on these pixels?
            indices = np.where(d0 == np.max(d0))
            __bgColor = self._Compositor__bgColor
            c0[indices[0], indices[1], 0] = __bgColor[0]
            c0[indices[0], indices[1], 1] = __bgColor[1]
            c0[indices[0], indices[1], 2] = __bgColor[2]

        return c0
