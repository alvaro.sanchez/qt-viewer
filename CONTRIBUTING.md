Contributing to Cinema
======================

This page documents at a very high level how to contribute to Cinema.

Workflow
========

Cinema development uses a [gitflow workflow][gitflow] based on a develop branch and different topic branches.  Develop always reflects a state with the latest delivered development changes and is the place from where topic branches are branched form and merged to (develop can be seen as the integration branch and is semi-stable).  Master contains only stable releases.

Our collaboration workflow consists of three main steps:

1. Local Development:
    * Update
    * Create a Topic (branch from develop)

2. Code review:
    * Share a Topic
    * Create a merge request (to merge back to develop)
    * Review the merge request
    * Revise the Topic

3. Integrate Changes:
    * Merge a Topic branch (merge back to develop)
    * Delete the Topic

Release
-------

1. Develop is stable
2. Merge Develop to master

Notes
-----

*  Develop is an integration branch so all features need to be developed in a topic branch. Only minor changes can be applied directly in develop.

*  Hotfix branches need to branch from master (current release) and merged back to master and develop.

*  Release branches can be created if necessary. These should branch from devleop and merge back to master and develop.

* For more details see this [link.][gitflow]

[gitflow]: http://nvie.com/posts/a-successful-git-branching-model/
