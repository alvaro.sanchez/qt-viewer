#!/usr/bin/python
import sys
import os

from PySide.QtCore import *
from PySide.QtGui import *

if os.path.exists("./widgets/uiFiles"):
  # compile ui files as long as the dir exists
  import pysideuic as uic
  uic.compileUiDir("./widgets/uiFiles")

from MultiViewerMain import *
from common import DatabaseLoader


# ---------------------------------------------------------------------
# create the qt app
a = QApplication(sys.argv)
v = ViewerMain()

# parse arguments if any
argv = sys.argv
if len(argv) > 1:
    databases = DatabaseLoader.loadAll(sys.argv[1])
    v.setStores(databases)

## ------------------- start profiling -------------------------------
#import cProfile, pstats, StringIO
#pr = cProfile.Profile()
#pr.enable()
## -------------------------------------------------------------------

v.show()
qAppRet = a.exec_()

## --------------------- finish profiling ----------------------------
#pr.disable()
#s = StringIO.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats(0.1) # print 10% of calls
#print s.getvalue()
## -------------------------------------------------------------------

sys.exit(qAppRet)
