#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import itertools

from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtGui import QItemSelectionModel as QISM
import PIL

from cinema_python.LayerSpec import *
from cinema_python.QueryTranslator import *
from cinema_python.QueryTranslator_SpecB import *
from rendering.LookupTableManager import *
from widgets.uiFiles import ui_ViewerMain as ui
from widgets import SliderBox
from widgets.PipelineModel import *
from common.CinemaSpec import *


class ViewerMain(QMainWindow):
    def __init__(self, parent = None):
        super(ViewerMain, self).__init__(parent)
        self.__initializeViewer()

    def __initializeViewer(self):
        self._plModel = PipelineModel()
        self._ui = ui.Ui_ViewerMain()
        self._colorPicker = QColorDialog(self)
        self.__fileDialog = QFileDialog(self, "Open a Cinema Database", "~", "JSON Input Files (info.json)")
        self._currentQuery = None
        self._sliderParams = {} # keeps convenient access to dynamically gen sliders

        # create ui
        self._ui.setupUi(self)
        self.__connectMenuActions()
        self._ui._parametersWidget.hide()
        self._ui._displayWidget.hide()

        # setup model and view
        self._ui._tviewLayers.setModel(self._plModel)
        tvHeader = self._ui._tviewLayers.header()
        tvHeader.swapSections(1, 0)
        tvHeader.resizeSection(1, 70)

    def __expandTreeView(self):
        root = self._plModel.index(0, 0, QModelIndex())
        allIndices = self._plModel.match(root, Qt.DisplayRole, "*", -1, Qt.MatchWildcard | Qt.MatchRecursive);
        for i in allIndices:
          self._ui._tviewLayers.expand(i);

    def __setDefaultSelection(self):
        # default = root model index
        index = self._plModel.index(0, 0, QModelIndex())
        self._ui._tviewLayers.selectionModel().setCurrentIndex(index, QISM.SelectionFlag.Select)

    def __connectMenuActions(self):
        self._ui._actionExit.triggered.connect(self.close)
        self._ui._actionOpen.triggered.connect(self.__onOpenFileTriggered)

    def __connectUiQueries(self):
        ''' Warning: this method does not connect the signals from 'range-value'
        sliders other than time/camera. Those are connected on creation from within
        self.__createRangeParameters. '''
        self._ui._tviewLayers.clicked.connect(self._plModel.onViewClicked)
        self._plModel.queryChanged.connect(self.__onQueryChanged)

        self._ui._sliderTime.queryChanged.connect(self.__onQueryChanged)
        self._ui._sliderPhi.queryChanged.connect(self.__onQueryChanged)
        self._ui._sliderTheta.queryChanged.connect(self.__onQueryChanged)

        self._ui._wColorBar.colorLutChanged.connect(self._onColorLutChanged)
        sm = self._ui._tviewLayers.selectionModel()
        sm.currentChanged.connect(self._plModel.onSelectedItemChanged)
        self._plModel.currentColorLutChanged.connect(self._ui._wColorBar.setCurrentLut)
        self._plModel.itemVisibilityChanged.connect(self.__enablePropertyWidgets)

        self._ui._pbBgColor.clicked.connect(self._onBgColorClicked)
        self._ui._pbSetGeometryColor.clicked.connect(self._onSetGeometryColor)

        disp = self._ui._displayWidget
        self._ui._cbUseGeometryColor.stateChanged.connect(disp.enableGeometryColor)
        self._ui._cbUseGeometryColor.stateChanged.connect(self._render)
        self._ui.cbUseLighting.stateChanged.connect(disp.enableLighting)
        self._ui.cbUseLighting.stateChanged.connect(self._render)
        self._plModel.valueRangeChanged.connect(self._ui._wColorBar.setMinMaxValues)

    def __onOpenFileTriggered(self):
        if self.__fileDialog.exec_() == self.__fileDialog.Accepted:
            filePath = self.__fileDialog.selectedFiles()[0] #QStringList
            self.__initializeViewer()
            cs = cinema_store.FileStore(filePath)
            cs.load()
            self.setStore(cs)

    @QtCore.Slot(bool)
    def __enablePropertyWidgets(self, enable):
        #self._ui._gbColorLut.setEnabled(enable)
        #self._ui._gbGeometryColor.setEnabled(enable)
        self._ui._gbColorLut.setVisible(enable)
        self._ui._gbGeometryColor.setVisible(enable)

    def _populateParameters(self, store, version):
        storeParameters = store.parameter_list
        keys = sorted(storeParameters)

        # special range keys that are most common
        keys_special = []
        for special in ['time','phi','theta']:
            if special in keys:
                keys.remove(special)
                keys_special.append(special)

        # setup special parameters if available
        if 'time' in keys_special:
            self._ui._sliderTime.configureSlider(storeParameters, 'time')

        if 'phi' in keys_special:
            self._ui._sliderPhi.configureSlider(storeParameters, 'phi')
            self._ui._displayWidget.initializePhiValues(storeParameters['phi'])
            self._ui._displayWidget.phiChanged.connect(self._ui._sliderPhi.onParameterChanged)
            self._ui._sliderPhi.parameterChanged.connect(self._ui._displayWidget.setPhi)

        if 'theta' in keys_special:
            self._ui._sliderTheta.configureSlider(storeParameters, 'theta')
            self._ui._displayWidget.initializeThetaValues(storeParameters['theta'])
            self._ui._displayWidget.thetaChanged.connect(self._ui._sliderTheta.onParameterChanged)
            self._ui._sliderTheta.parameterChanged.connect(self._ui._displayWidget.setTheta)

        #self._ui._displayWidget.disconnectInteractorSignals()
        if ('phi' in keys_special or 'theta' in keys_special):
            self._ui._displayWidget.connectInteractorSignals()

        # create other sliders for range parameters
        remainingKeys = self.__createRangeParameters(storeParameters, keys)

        # parse the remaining keys (if any) and include them in the PlModel
        if len(remainingKeys) > 0:
            self._plModel.setDefaultColorLut(self._ui._wColorBar.getCurrentColorLut())
            self._plModel.populate(store, remainingKeys, version)
            self.__expandTreeView()
        else:
            self._ui._tviewLayers.hide()

    # TODO this might be moved into its own Widget class in the future
    def __createRangeParameters(self, storeParameters, keys):
        remainingParameterKeys = []
        for k in keys:
            if storeParameters[k]['type'] == 'range':
               s = SliderBox.SliderBox(self._ui._wRangeParams)
               s.configureSlider(storeParameters, k)
               s.setTitle(k)
               s.queryChanged.connect(self.__onQueryChanged)
               self._ui._wRangeParams.layout().addWidget(s)
               self._sliderParams[k] = s

               if len(storeParameters[k]['values']) <= 1:
                  s.hide();
            else:
                remainingParameterKeys.append(k)

        if len(self._sliderParams) == 0:
                self._ui._saRangeParams.hide()

        return remainingParameterKeys

    def setStore(self, store):
        self._ui._parametersWidget.show()
        self._ui._displayWidget.show()

        # create a QueryTranslator instance depending on the data version
        version = CinemaSpec().resolveStoreVersion(store)

        if version == CinemaSpec.A:
            self._queryTranslator = QueryTranslator_SpecA()
            self._ui._gbColorLut.hide()
            self._ui._gbGeometryColor.hide()
            self._ui._pbBgColor.hide()
        elif version == CinemaSpec.B:
            self._queryTranslator = QueryTranslator_SpecB()
            self._ui._wColorBar.setCurrentLut("Spectral")

        self._ui._displayWidget.setSpecification(version)
        self._queryTranslator.setStore(store)
        self._populateParameters(store, version)
        self.__connectUiQueries()
        self.__setDefaultSelection()
        self.__onQueryChanged() # call it once to show init query (default values)

    @QtCore.Slot()
    def _onColorLutChanged(self):
        # this check is necessary given that _wColorBar.colorLutChanged() causes
        # a call to this slot even before being connected.
        if self._currentQuery == None:
          return

        clutStruct = self._ui._wColorBar.getCurrentColorLut()
        sm = self._ui._tviewLayers.selectionModel()
        self._plModel.setColorLut(sm.currentIndex(), clutStruct)
        self._render()

    def _onBgColorClicked(self):
        if self._colorPicker.exec_() == self._colorPicker.Accepted:
            color = self._colorPicker.selectedColor()
            self._ui._displayWidget.setBackgroundColor(color)
            self._render()

    def _onSetGeometryColor(self):
        if self._colorPicker.exec_() == self._colorPicker.Accepted:
            sm = self._ui._tviewLayers.selectionModel()
            self._plModel.setGeometryColor(sm.currentIndex(), self._colorPicker.selectedColor().getRgb())
            self._render()

    @QtCore.Slot()
    def __onQueryChanged(self):
        totalQuery = dict()

        # add static (base) queries
        queryPhi = self._ui._sliderPhi.getQuery()
        if queryPhi:
            totalQuery.update(queryPhi)

        queryTheta = self._ui._sliderTheta.getQuery()
        if queryTheta:
            totalQuery.update(queryTheta)

        queryTime = self._ui._sliderTime.getQuery()
        if queryTime:
            totalQuery.update(queryTime)

        # add parameter model queries
        queryPlModel = self._plModel.getQuery()
        if queryPlModel:
            totalQuery.update(queryPlModel)

        self._addOtherSliderQueries(totalQuery)

        self._currentQuery = totalQuery
        self._render()

    def _addOtherSliderQueries(self, totalQuery):
        if len(self._sliderParams) > 0:
            for paramName, slider in self._sliderParams.iteritems():
                q =  slider.getQuery()
                if q:  totalQuery.update(q)

    def _render(self):
        #print "\n->>> _render: Total query: ", self._currentQuery, "\n"

        # translate GUI choices (self._currentQuery) into a set of queries (a.k.a. LayerSpec instances) that we need to render with
        layers = self._queryTranslator.translateQuery(self._currentQuery)

        try:
            c = self._ui._displayWidget.compositor
            c.setColorDefinitions(self._plModel.getColorDefinitions())
            c0 = c.render(layers)
        except IndexError:
            # handles the case when there are no layers selected (Compositor throws)
            # or layers were not loaded correctly
            self._ui._displayWidget.renderView().clear()
            return

        # show the result
        pimg = PIL.Image.fromarray(c0)
        # Pillow for OS X has replaced "tostring" with "tobytes"
        try:
            imageString = pimg.tostring('raw', 'RGB')
        except Exception:
            imageString = pimg.tobytes('raw', 'RGB')
        qimg = QImage(imageString, pimg.size[0], pimg.size[1], pimg.size[0]*3, QImage.Format_RGB888)
        pix = QPixmap.fromImage(qimg)

        # Try to resize the display widget
        self._ui._displayWidget.renderView().sizeHint = pix.size
        self._ui._displayWidget.renderView().setPixmap(pix)
